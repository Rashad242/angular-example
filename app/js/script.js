var videoController = angular.module( 'videoClipper', 
    [
        "ngSanitize",
        "com.2fdevs.videogular",
        "com.2fdevs.videogular.plugins.controls"

    ]
)
.filter('savedClipFilter', function() {
    return function(input) {
        len=input.length;
        for (var i=0; i<len; i++) {
            if (input[i].isSaved !== true) {
                input.push(input[i]);
            }
        }

        return input;
    }
})
.filter('searchFilter',[function () {
    return function (object) {
        var array = [];
        angular.forEach(object, function (element) {
            if (array.indexOf(element) === -1) {
                array.push(element);
            }
        });

        return array;
    };
}
]);

videoController.service('videoClips', ["$filter", function ($filter) {
    var clips = [];
    var savedClips = [];
    var clipCounter = 0;
    var currentTime;
    var w, h, ratio, canvas, context, imgJpeg;

    function captureVideoImage() {
        video = document.querySelector('video');
        canvas = document.createElement('canvas');
        context = canvas.getContext('2d');

        ratio = video.videoWidth / video.videoHeight;
        w = video.videoWidth - 100;
        h = parseInt(w / ratio, 10);

        canvas.width = w;
        canvas.height = h;      

        context.fillRect(0, 0, w, h);
        context.drawImage(video, 0, 0, w, h);

        //draw image to canvas. scale to target dimensions
        context.drawImage(video, 0, 0, canvas.width, canvas.height);

        //convert to desired file format
        imgJpeg = canvas.toDataURL('image/jpeg'); // can also use 'image/png'
    }

    function addClip(clipData) {
        if (clipData.title) {
            clipContainer = 'clip_' + clipCounter;

            currentTime = clipData.startTime;

            clips.push({
                class: clipContainer,
                title: clipData.title,
                startTime: clipData.startTime, 
                stopTime: clipData.stopTime,
                sourcePath: clipData.sourcePath,
                imgSrc: imgJpeg,
                tags: clipData.tags,
                isSaved: false
            });
            
            clipCounter++;
        }
    }

    function updateLocalStorageItems() {
        var savedClips = $filter('savedClipFilter')(clips);
        localStorage.setItem('savedClips', JSON.stringify(savedClips));
    }

    function getLocalStorageItem() {
        return JSON.parse(localStorage.getItem('savedClips'));
    }

    // Retrieve the object from storage
    var retrievedObject = getLocalStorageItem();
    if (retrievedObject !== null) {
        clips = $filter('searchFilter')(retrievedObject);;
        clipCounter = clips.length;
    }


    return {
        getClips: function (index) {
            if (typeof index !== 'undefined') {
                return clips[index];
            }
            return clips;
        },
        removeClip: function (item) {
            var index = clips.indexOf(item);
            clips.splice(index, 1);

            updateLocalStorageItems();
        },
        getNextClip: function (item) {
            var index = clips.indexOf(item);
            if(index >= 0 && index < clips.length - 1) {
               nextItem = clips[index + 1];
            } else {
                nextItem = clips[0];
            }


           return nextItem;
        },
        getPreviousClip: function (item) {
            var index = clips.indexOf(item);
            if(index == 0) {
               nextItem = clips[clips.length - 1];
            } else {
                nextItem = clips[index - 1];
            }


           return nextItem;
        },
        updateClip: function (item, new_item) {
            var index = clips.indexOf(item);

            clips[index]['title'] = new_item.title;
            clips[index]['startTime'] = new_item.startTime;
            clips[index]['stopTime'] = new_item.stopTime;
            clips[index]['tags'] = new_item.tags;

            if (item.isSaved) {
                updateLocalStorageItems()
            }
        },
        captureScreenShot: function () {
            captureVideoImage();
        },
        updateLocalStorageItems: function () {
            updateLocalStorageItems();
        },
        getSeekTime: function () {
            return currentTime;
        },
        addClip: function(clipData) {
            addClip(clipData);
        }
    }
}]);

videoController.controller('videoCtrl', ["$sce", "$scope", "videoClips", "$timeout", function ($sce, $scope, videoClips, $timeout, $filter) {
    var controller = this;
    controller.API = null;

    $scope.clips = [];
    $scope.invalidData = true;
    $scope.autoPlayTimer = null;
    $scope.autoPlayClips = false;
    $scope.currentClip = null;
    $scope.isEditingClip = false;
    $scope.isCuttingClip = false;
    $scope.countDownTimer = 0;
    $scope.showCountDownTimer = false;

    startTime = null;
    stopTime = null;
    totalTime = null;


    $scope.mainVideo = [
        {
            sources: [
                {src: $sce.trustAsResourceUrl("main-video.mp4#t=0"), type: "video/mp4"}
            ]
        }
    ];

    controller.config = {
        preload: "auto",
        sources: $scope.mainVideo[0].sources,
        theme: {
            url: "http://www.videogular.com/styles/themes/default/latest/videogular.css"
        }
    };

    $scope.$watch(
        function () { 
            return (typeof $scope.clipTitle !== 'undefined') 
                && ($scope.clipTitle !== '') 
                && (parseFloat($scope.startTime, 10) >= 0) 
                && (parseFloat($scope.stopTime, 10) > parseFloat($scope.startTime, 10))
                && (parseFloat($scope.stopTime, 10) <= parseFloat(totalTime, 10));
        }, 
        function (isValid) {
            $scope.invalidData = !isValid;
        }
    );

    $scope.$watch(
        function () { 
            return videoClips.getClips();
        }, 
        function (clips) {
            $scope.clips = clips;
        }
    );

    $scope.$watch(
        function () { 
            return $scope.autoPlayClips;
        }, 
        function (autoPlay) {
            if (!autoPlay) {
                $scope.showCountDownTimer = false;
                clearInterval($scope.autoPlayTimer);
            }
        }
    );

    $scope.$watch(
        function () { 
            return $scope.isEditingClip;
        }, 
        function (isEditingClip) {
            if (isEditingClip === true) {
                $scope.pageTitle = "Edit Clip";
            } else {
                $scope.pageTitle = "Media Clipper";
            }
        }
    );

    $scope.$watch(
        function () { 
            return controller.API.currentState;
        }, 
        function (videoState) {
            if (videoState !== 'play' && $scope.autoPlayClips && (controller.currentTime >= $scope.currentClip['stopTime'])) {
                $scope.showCountDownTimer = true;
                var counter = 3;
                $('#countDownTimer').text(counter);
                clearInterval($scope.autoPlayTimer);
                $scope.autoPlayTimer = setInterval(function(){
                     $('#countDownTimer').text(--counter);
                     if (counter == 0) {
                        $scope.playNextVideo();
                     }
                }, 1000);
            } else {
                if ($scope.isEditingClip && $scope.currentClip != null && (controller.currentTime >= $scope.currentClip['stopTime'])) {
                    controller.API.seekTime($scope.currentClip['startTime']);
                }
                clearTimeout($scope.autoPlayTimer);
            }
        }
    );

    $scope.playNextVideo = function () {
        if ($scope.currentClip != null) {
             nextClip = videoClips.getNextClip($scope.currentClip);
             $scope.editClip(nextClip);
             $scope.setVideo(nextClip);
        }
        $scope.showCountDownTimer = false;
    }

    $scope.playPreviousVideo = function () {
        if ($scope.currentClip != null) {
             nextClip = videoClips.getPreviousClip($scope.currentClip);
             $scope.editClip(nextClip);
             $scope.setVideo(nextClip);
        }
        $scope.showCountDownTimer = false;
    }
    
    $scope.onPlayerReady = function(API) {
        controller.API = API;
    };

    $scope.manualAddClip = function() {
        if (typeof $scope.startTime != undefined) {
            $scope.currentClip = null;
            controller.API.seekTime($scope.startTime);
            videoClips.captureScreenShot();

            videoClips.addClip({
                title: $scope.clipTitle,
                tags: $scope.clipTags, 
                startTime: $scope.startTime, 
                stopTime: $scope.stopTime,
                sourcePath: "main-video.mp4#t=" + $scope.startTime +',' + $scope.stopTime
            });

            $scope.clipTitle = '';
            $scope.startTime = '';
            $scope.stopTime = '';
        }
    };

    $scope.updateTime = function (currentTime, totalPlayTime) {
        controller.currentTime = currentTime;
        totalTime = totalPlayTime;
    };

    $scope.playMainVideo = function(fromBeginning){
        controller.config.sources = $scope.mainVideo[0].sources;
        if (fromBeginning) {
            controller.API.seekTime(0);
        }

        clearInterval($scope.autoPlayTimer);
        controller.API.play();

        $scope.isEditingClip = false;
        $scope.totalTime = totalTime;

        $scope.clipTitle = '';
        $scope.startTime = '';
        $scope.stopTime = '';
        $scope.clipTags = '';
    };

    $scope.newClipperTimer = function(){
        if ($sce.capturingClip) {
            return;
        }
        
        controller.API.play();
        videoClips.captureScreenShot();
        $scope.isCuttingClip = true;
        startTime = controller.currentTime;
        stopTime = null;
    };

    $scope.stopClipperTimer = function(){
        controller.API.pause();
        $scope.isCuttingClip = false;
        stopTime = controller.currentTime;

        // Probably would be better with a jquery modal to enter name and possibly additional fields
        var customName = prompt("Enter clip name");
        
        clipname = 'n/a';
        if (customName != null) {
            clipname = customName;
        }

        videoClips.addClip({
            title: customName,
            startTime: startTime, 
            stopTime: stopTime,
            tags: '',
            sourcePath: "main-video.mp4#t=" + startTime +',' + stopTime
        });
    };

    $scope.clearLocalStorageItems = function(clip) {
        localStorage.removeItem('savedClips');
    }

    $scope.saveClip = function(clip) {
        clip['isSaved'] = true;
        videoClips.updateLocalStorageItems();
    };

    $scope.removeClip = function(clip) {
       if (confirm('Are you sure you want to remove this clip?')) {
            videoClips.removeClip(clip);
        }
    };

    $scope.searchFilter = function (item) { 
        return item.tags.match(/^\$scope.clipTagSearchText/) ? true : false;
    };

    $scope.updateClip = function(newClip) {
       if (confirm('Are you sure you want to update this clip?')) {
            newClip['title'] = $scope.clipTitle;
            newClip['tags'] = $scope.clipTags;
            newClip['startTime'] = $scope.startTime;
            newClip['stopTime'] = $scope.stopTime;
            newClip['sourcePath'] = "main-video.mp4#t=" + $scope.startTime +',' + $scope.stopTime;

            videoClips.updateClip($scope.currentClip, newClip);
        }
    };

    $scope.editClip = function(clip) {
        $scope.isEditingClip = true;
        $scope.currentClip = clip;

        $scope.clipTitle = clip['title'];
        $scope.clipTags = clip['tags'];
        $scope.startTime = clip['startTime'];
        $scope.stopTime = clip['stopTime'];

    };

    $scope.setVideo = function(clip) {
        controller.API.stop();
        $scope.currentClip = clip;
        console.log(clip.sourcePath);
        controller.config.sources = [
            {src: $sce.trustAsResourceUrl(clip.sourcePath), type: "video/mp4"},
        ];
        $timeout(controller.API.play.bind(controller.API), 100);
    };

    $scope.playClip = function(clip) {
        $scope.editClip(clip);
        clearInterval($scope.autoPlayTimer);

        var stopVideoAfter = (clip['stopTime'] - clip['startTime']) * 1000;
        $scope.setVideo(clip);
    };


    var keyPress=function(e) {
        var keyCode = e.keyCode || e.charCode;
        switch ( keyCode ) {
            case 13 :  // return
            case 32 :  // space
                if ($scope.currentClip != null) {
                    $scope.setVideo($scope.currentClip);
                }
                break;
        }
    }

    var keyDown=function(e) {
        var keyCode = e.keyCode || e.charCode;
        switch ( keyCode ) {
            case 37 :  // left arrow
                $scope.playPreviousVideo();
                break;
            case 39 :  // right arrow
                $scope.playNextVideo();
                break;
        }
    }

    $('body').bind({
        keydown: function(e) { keyDown(e); },
        keypress: function(e) { keyPress(e); }
    });

}]);

function sticky_relocate() {
    var window_top = $(window).scrollTop();
    var div_top = $('#sticky-anchor').offset().top;
    if (window_top > div_top) {
        $('#sticky').addClass('stick');
    } else {
        $('#sticky').removeClass('stick');
    }
}

$(function () {
    $(window).scroll(sticky_relocate);
    sticky_relocate();
});